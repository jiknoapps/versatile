module.exports = () => {
	return {
		clientDir: `client`,
		serverDir: `server`,
		assetsDir: `assets`,
		serverEntry: `server.js`,
		desktopEntry: `desktop.js`,
		serverBasePath: `api`,
		clientBasePath: `/`,
		assetsBasePath: `/`,
		template: `default`,
		browserDependencies: [],
		osDependencies: [],
		appName: `versatile-app`,
		appID: `versatile-app`,
		productName: `Versatile App`,
		backgroundColor: `#2e2c29`,
		desktopFiles: [],
		category: 'developer-tools',
		iconsDir: `node_modules/versatilejs/icons`,
	};
};
