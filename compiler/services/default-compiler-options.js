module.exports = {
	useConfig: false,
	configPath: `versatile.config.js`,
	path: `dist`,
	platform: `browser`,
};
