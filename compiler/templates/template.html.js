module.exports = `<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Loading...</title>

		%versatile_styles%
	</head>

	<body>
		%versatile_app%

		%versatile_scripts%
	</body>
</html>`;
